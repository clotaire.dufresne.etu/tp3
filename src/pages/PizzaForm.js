import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = document.querySelector('form');
		form.addEventListener('submit', event => this.submit(event));
	}

	submit(event) {
		event.preventDefault();
		const target = event.target, input = target.querySelector('input[name=name]').value;
		if(input === "") {
			window.alert("Nom vide !");
		}
		else {
			window.alert(`La pizza ${input} a été ajoutée`);
			target.reset();
		}
	}
}
