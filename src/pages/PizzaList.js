import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Page from './Page.js';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList');// on passe juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
