import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

pizzaList.pizzas = data;
Router.navigate(document.location.pathname); // affiche la liste des pizzas

document.querySelector('.newsContainer').setAttribute('style', '');

window.onpopstate = () => {
	Router.navigate(document.location.pathname, true);
};