export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;

	static navigate(path, noPush) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
			const d = document.querySelectorAll('.mainMenu a');
			d.forEach(element => {
				if (element.textContent == route.title) element.classList.add('active');
				else element.classList.remove('active');
			});
			if(!noPush)
				window.history.pushState(null, null, route.path);
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		element.addEventListener('click', event => {
			event.preventDefault();
			const target = event.target;
			Router.navigate(target.getAttribute('href'));
		});
	}
}
