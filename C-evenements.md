<img src="images/readme/header-small.jpg" >

# C. Les événements de l'API DOM <!-- omit in toc -->

_**Maintenant que l'on sait sélectionner et modifier des éléments de la page HTML, voyons comment réagir aux actions de l'utilisateurs avec les événements et notamment comment mettre en place un système de navigation dans notre application.**_

## Sommaire <!-- omit in toc -->
- [C.1. Rappels](#c1-rappels)
- [C.2. Navigation en JS : afficher/masquer un élément](#c2-navigation-en-js-affichermasquer-un-élément)
- [C.2. Navigation en JS : Le menu](#c2-navigation-en-js-le-menu)


## C.1. Rappels
**Le système d'événements en JS permet de réagir à des actions de l'utilisateur (_survol d'un élément, click sur un lien, soumission d'un formulaire, etc._) ou à des événements déclenchés par le navigateur (_fin du chargement de la page ou d'une image, etc._).**

Comme vu en cours (cf. pdf du cours sur moodle) on peut **associer une fonction à un événement grâce à la méthode [`addEventListener()`](https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener)** de la classe `Element`.

Par exemple, pour déclencher une fonction nommée `handleClick` lors du clic sur le premier lien de la page, on peut écrire :
```js
function handleClick( event ) {
	event.preventDefault(); // empêche le rechargement de la page
	console.log(event);
}
const link = document.querySelector('a'); // sélectionne le premier lien de la page
link.addEventListener('click', handleClick); // écoute l'événement
```

**Notez que comme vu en cours :**
1. le 2e paramètre que l'on passe à addEventListener est une **référence de la fonction `handleClick`** (_son nom_) et pas l'exécution de la fonction (`handleClick()` _avec les parenthèses_)
2. la fonction qui est passée à `addEventListener()` **reçoit automatiquement en paramètre un objet de type [`Event`](https://developer.mozilla.org/en-US/docs/Web/API/Event)**
3. Il faut presque systématiquement (_sauf cas très particuliers_) **appeler en premier lieu la méthode `event.preventDefault()`** : cette méthode permet d'éviter que le navigateur n'exécute le traitement par défaut de l'événement (par exemple rediriger l'utilisateur vers une nouvelle page lorsqu'il clique sur un lien, recharger la page lorsqu'il soumet un formulaire, etc.).
4. **Si vous êtes dans une classe, prenez garde aux problèmes de scope** liés à `addEventListener()` (_`this` est toujours l'élément HTML qui a déclenché l'événement_) et préférez l'emploi de arrow functions en inline comme ceci :
	```js
	myElement.addEventListener('click', event => {
		// ici pas de problème de scope, `this` est préservé
		event.preventDefault();
		this.monAutreMethode(); // ok
	});
	```

## C.2. Navigation en JS : afficher/masquer un élément
**Il existe plusieurs façons de gérer la navigation en JS.**

**On peut soit générer du code HTML entièrement en JS et l'injecter dans la page (_comme on le fait déjà pour la `PizzaList`_) soit se contenter d'afficher/masquer des portions de la page déjà présentes dans le code html.** \
**C'est cette technique que l'on va maintenant travailler.**

1. **Inspectez le code du fichier `index.html`** : vous remarquerez qu'après la balise `<header>` se trouve une balise masquée (_avec `display:none`_) :
	```html
	<section class="newsContainer" style="display:none">...</section>
	```
2. **Dans `src/main.js` rendez cette section visible** à l'écran à l'aide de la méthode `setAttribute()`.

	> _**NB1 :** Pour afficher une balise qui est en `display:none`, vous pouvez remplacer la valeur du style `display` par `''` (chaîne vide)._

	> _**NB2 :** Pour manipuler les styles vous pouvez aussi utiliser la propriété [`myElement.style` _(mdn)_](https://developer.mozilla.org/en-US/docs/Web/API/CSS_Object_Model/Using_dynamic_styling_information#modify_an_element_style) qui permet d'agir sur l'attribut `style="..."` de manière un peu plus simple._

	> _**NB3 :** Plus "bourrin" mais qui peut fonctionner aussi dans ce cas là, il existe aussi une méthode [`myElement.removeAttribute()` _(mdn)_](https://developer.mozilla.org/fr/docs/Web/API/Element/removeAttribute)..._

	<img src="images/readme/newscontainer.png" >

3. A**u clic sur le bouton `<button class="closeButton"></button>` (_contenu dans la section_) masquez à nouveau le bandeau de news.**


## C.2. Navigation en JS : Le menu

La technique de navigation vue à l'instant (_masquer/afficher des balises_) est pratique pour des applications simples, où tout le contenu HTML est déjà généré côté serveur (_en JAVA, en PHP, en C# ou encore avec Node.JS_). \
**En revanche elle n'est pas très adaptée aux SPA où en général on a du contenu dynamique à injecter dans la page.**

On va donc revenir à la principale technique de navigation employée dans les SPA, celle que l'on utilisait jusque là : **générer dynamiquement, en JS, le code HTML de la page en fonction de ce que demande l'utilisateur** (_comme on le faisait avec la `PizzaList` par exemple_).

Pour approfondir cette technique de navigation et **permettre de passer d'une page à une autre**, je vous propose de nous appuyer sur la classe `Router` que vous avez développée lors du précédent TP ([TP2 / D.3. Propriétés et méthodes statiques : La classe Router](https://gitlab.univ-lille.fr/js/tp2/-/blob/master/D-poo-avancee.md#d3-propri%C3%A9t%C3%A9s-et-m%C3%A9thodes-statiques-la-classe-router)) et dont ma version se trouve dans ce repo (`src/Router.js`).

**L'objectif de l'exercice ici est simple : faire en sorte que lorsque l'utilisateur clique sur un des liens du menu, on affiche un contenu différent dans la page grâce à la méthode `Router.navigate()`.**

<img src="images/readme/nav-simple.gif">

1. **Dans `src/main.js`, commencez par créez des `Component` pour les différents liens du menu :**

	```js
	const pizzaList = new PizzaList([]),
		aboutPage = new Component('section', null, 'Ce site est génial'),
		pizzaForm = new Component('section', null, 'Ici vous pourrez ajouter une pizza');
	```
	> _**NB1 :** `pizzaList` existe déjà, on ajoute ici juste `aboutPage` et `pizzaForm`._

	> _**NB2 :** pour le moment on utilise pour ces 2 nouvelles pages des `Component` très simples, "en dur", mais on les passera dans des classes spécifiques plus tard._

	**Puis ajoutez les routes correspondantes dans notre Router :**
	```js
	Router.routes = [
		{ path: '/', page: pizzaList, title: 'La carte' },
		{ path: '/a-propos', page: aboutPage, title: 'À propos' },
		{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
	];
	```

2. **Pour détecter le clic sur les liens du menu, plutôt que de mettre le code dans le `main.js`, on va déléguer ce travail au `Router`.**

	> _**NB :** Faire ça dans le `Router` permet de **centraliser** tout ce qui concerne la navigation : la détection du clic + le mécanisme de changement de page en lui-même (`Router.navigate()`). Quelle bonne idée !_

	Dans le `src/main.js`, ajoutez la ligne suivante :

	```js
	Router.menuElement = document.querySelector('.mainMenu');
	```
	> _**NB :** en faisant cela on envoie au Router une **référence vers la balise `<ul class="mainMenu">`** qui contient le menu de navigation (cela nous évitera de faire référence à `document` dans la classe `Router` et en plus ça reste cohérent avec le fonctionnement des propriétés `titleElement` et `contentElement`)_

	Router.menuElement est en fait un "setter" dont je vous fourni le code de base à compléter (_à coller **dans** le corps de la classe `Router`_) :
	```js
	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
	}
	```

	À l'aide de ce setter, **détectez le clic sur n'importe quel lien du menu** (_actuellement il n'y a en a que 3, mais votre code doit fonctionner quelque soit le nombre de liens_) et **affichez dans la console l'attribut `href` du lien qui a été cliqué**. \
	Par exemple si l'utilisateur clique sur le lien **"À propos"** la console doit afficher la chaîne de caractères **`"/a-propos"`**

	> _**NB1 :** vous aurez besoin pour celà de la propriété [`event.currentTarget` _(mdn)_](https://developer.mozilla.org/fr/docs/Web/API/Event/currentTarget) et de la méthode [`element.getAttribute()` _(mdn)_](https://developer.mozilla.org/fr/docs/Web/API/Element/getAttribute)_

	> _**NB2 :** en cas de **problème de scope**, relisez donc la fin du paragraphe [C.1. Rappels](#c1-rappels), juste au cas où..._

3. **Pour terminer, maintenant que vous avez récupéré le `href` du lien cliqué, il ne vous reste plus qu'à invoquer la méthode `Router.navigate()` en lui passant en paramètre le `href` en question !**

	> _**NB :** Là aussi, si vous avez des difficultés à appeler `Router.navigate()` pour des questions **de scope**, relisez la fin du paragraphe [C.1. Rappels](#c1-rappels)..._

	Vérifiez que votre code fonctionne : quand l'utilisateur clique sur un lien du menu, **le contenu de la route correspondante doit s'afficher dans la page !**



## Étape suivante <!-- omit in toc -->
Maintenant que l'on est capable de détecter les actions de l'utilisateur et de modifier la page HTML en conséquence, attaquons nous à la gestion des formulaires : [D. Les formulaires](./D-formulaires.md).